#Item 1#
Contextual retical helps to orient in space by showing the centre of focus.
#Item 2#
Interacting with objects — when the user turns his/her attention to an interactive object the reticle should react accordingly.
##Source for 1&2##
https://medium.com/@LeapMotion/vr-design-best-practices-bb889c2dc70
#Item 3#
Limit unintended interactions making sure that interactions are well spaced so not to trigger multiple elements.
#Item 4#
Make sure text is readable and that images are legible.
#Item 5#
Interactive elements should be in the “goldilock zone” of height which is between desk height and eye height.
##Source for 3,4 and 5##
https://unity3d.com/learn/tutorials/topics/virtual-reality/user-interfaces-vr?playlist=22946
#Item 6#
Having a spatial UI (UI tied to the world not the HUD) will allow players to focus on on the UI itself.
#Item 7#
Scale objects to the appearance of around 1.5M away for a comfortable expiernce
#Item 8#
Text related to objects should be within the same focus space and within the same distance.
##Source for 6,7 and 8##
https://virtualrealitypop.com/designforroomscalevr-a41e646444e7

Andrews displayed a few of his design principals effectively in his project. His sliders have adhered to his 5th design principal of locating interactive objects within desk and eye heights. He also has provided a spacial UI which allowed 
the ability to focus on the UI provided as to increase focus on the project. The text provided is also within the same space and distance which creates an aestetically pleasing design. Overall Andrew created a well thoughtout and designed
project which allowed for an overall enjoyable experience. 
Taylor Inglis


